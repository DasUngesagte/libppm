#include "libppm.h"

/**
 * Creates a PPM_Image.
 */
PPM_Image* createPPM(Format format, unsigned width, unsigned height, unsigned maxval, unsigned char *data) {

    // Allocate space on heap
    PPM_Image *ppm = (PPM_Image *) malloc(sizeof(PPM_Image));

    // Set values
    ppm->format = format;
    ppm->image_width = width;
    ppm->image_height = height;
    ppm->maxval = maxval;
    size_t size = width * height * (format == P3 || format == P6 ? 3 : 1);
    ppm->data = (unsigned char *) malloc(size);
    memcpy(ppm->data,data,size);

    return ppm;
}

/**
 * Deletes the given PPM_Image.
 */
void deletePPM(PPM_Image *ppm) {

    // Free data first
    free(ppm->data);
    free(ppm);
}

/**
 * Checks if next line is a comment and returns 0 if true, 1 else.
 */
int isComment(FILE *file) {
    int c = fgetc(file);
    ungetc(c, file);

    if (c == '#') {
        return 1;
    } else {
        return 0;
    }
}

void jumpComment(FILE *file) {
    int c;
    while ((c = fgetc(file)) != '\n')
    ;

    if (isComment(file)) {
        jumpComment(file);
    }
}

/**
 * Loads the image from the given path
 */
PPM_Image* loadPPM(const char *path) {
    FILE *image = fopen(path, "rb");

    if (!image) {
        printf("%s %s\n", "Could not open", path);
        return NULL;
    }

    // PPM Image values
    unsigned char format_val;
    Format format;
    unsigned image_width = 0;
    unsigned image_height = 0;
    unsigned maxval;

    // Return count checking
    int count;

    // Get Imageformat P1, P2, ...
    if (isComment(image)) {
        jumpComment(image);
    }

    count = fscanf(image, "P%hhd\n", &format_val);
    if (count != 1) {
        printf("%s\n", "Error while getting image format");
        return NULL;
    }
    format = format_val;

    // Get image dimensions
    if (isComment(image)) {
        jumpComment(image);
    }
    count = fscanf(image, "%u %u\n", &image_width, &image_height);
    if (count != 2) {
        printf("%s\n", "Error while getting image dimensions");
        return NULL;
    }

    // Getting maxval if format is one of P2,P3,P5,P6
    if (isComment(image)) {
        jumpComment(image);
    }

    if (format != P1 && format != P4) {
        count = fscanf(image, "%u[^\n]", &maxval);
        fgetc(image);   // read `\n` afterwards (fix for pixelshift if first pixel has values under 0x20

        if (count != 1) {
            printf("%s\n", "Error while getting image depth");
        }
    } else {
        maxval = 0;
    }

    /*
     * Error on some images
     *
    if (isComment(image)) {
        jumpComment(image);
    }
    */

    // Get image pixel
    unsigned num_pixel = image_width * image_height * (format == P3 || format == P6 ? 3 : 1);
    unsigned char *data = (unsigned char*) malloc(num_pixel);

    for (size_t i = 0; i < num_pixel; ++i) {
        if (format > P3) {
            data[i] = fgetc(image);
        } else {
            fscanf(image, "%hhd", &data[i]);
        }
    }

    // Create ppm image
    PPM_Image *ppm = createPPM(format, image_width, image_height, maxval, data);
    fclose(image);

    return ppm;
}


/**
 * Saves the image to the given path
 */
void savePPM(const char *path, PPM_Image *ppm) {
    FILE *image;
    //if (ppm->format > P3)
    image = fopen(path, "wb");  // TODO: needed?
    //else
    //	image = fopen(path, "w");

    /* magic number */
    fprintf(image,"P%d\n", ppm->format);

    /* size and depth */
    fprintf(image, "%d %d\n", ppm->image_width, ppm->image_height);

    if (ppm->format != P1 && ppm->format != P4)
        fprintf(image, "%d\n", ppm->maxval);

    /* data */
    /* get # of values*/
    unsigned max = ppm->image_width * ppm->image_height * (ppm->format == P3 || ppm->format == P6 ? 3 : 1);

    /* is image binary? (aka P4 to P7)*/
    if (ppm->format < P4) {
    /* write numbers as ASCII */
        for (unsigned i = 0; i < max; ++i) {
            fprintf(image, "%d ", ppm->data[i]);
            if (i != 0 && i % 16 == 0)
                fprintf(image, "\n");
            }
    } else {
        /* write as binary */
        //for (unsigned i = 0; i < max; ++i) {
        //    fputc(ppm->data[i], image); 
        //}
        fwrite(&(ppm->data[0]), sizeof(unsigned char), max, image);
    }

    fclose(image);
}

#if LIBPPM_DEBUG
int main(int argc, char **argv) {
    unsigned char data[9] = {0,0,255,0,255,0,255,0,0}; 

    PPM_Image image1 = {P3, 1, 3, 255, data};
    PPM_Image image2 = {P6, 1, 3, 255, data};

    printf("Saving images...\n");
    savePPM("./test_out_P3.ppm", &image1);
    savePPM("./test_out_P6.ppm", &image2);

    printf("Reading images...\n");
    PPM_Image *image3 = loadPPM("./test_out_P3.ppm");
    PPM_Image *image4 = loadPPM("./test_out_P6.ppm");
    printf("Re-read P3:\n\tFormat:\tP%d\n\tDimension:\t%d x %d\n",
        image3->format, image3->image_width, image3->image_height);
    printf("Re-read P6:\n\tFormat:\tP%d\n\tDimension:\t%d x %d\n",
        image4->format, image4->image_width, image4->image_height);

    if (argc > 1) {
        PPM_Image *test = loadPPM(argv[1]);
        printf("Read Comment-Test:\n\tFormat:\tP%d\n\tDimension:\t%d x %d\n",
            test->format, test->image_width, test->image_height);
    }
}
#endif
