#ifndef LIBPPM_H
#define LIBPPM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>    /* file handling */
#include <stdlib.h>
#include <string.h>    /* memory handling */

#define PPM_BYTES 0
#define PPM_TEXT 1

typedef enum {
    P1 = 1, /* ASCII Bitmap */
    P2, /* ASCII Greymap */
    P3, /* ASCII RGB */
    P4, /* Binary Bitmap */
    P5, /* Binary Greymap */
    P6, /* Binary RGB */
    P7  /* Binary Anymap*/
} Format;

typedef struct {
    Format format;
    unsigned image_width;
    unsigned image_height;
    unsigned maxval;
    unsigned char *data;
} PPM_Image;

/**
 * Creates a PPM_Image.
 */
PPM_Image *createPPM(Format format, unsigned width, unsigned height, unsigned maxval, unsigned char *data);

/**
 * Deletes the given PPM_Image.
 */
void deletePPM(PPM_Image *ppm);

/**
 * Loads the image at the given path.
 */
PPM_Image *loadPPM(const char *path);

/**
 * Saves the image into the given path.
 */
void savePPM(const char *path, PPM_Image *ppm);

#ifdef __cplusplus
};
#endif

#endif
