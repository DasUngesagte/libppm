CC=gcc
FLAGS=-Wall -DLIBPPM_DEBUG=1
LIBS=

.PHONY: clean

libppm: libppm.o
	$(CC) $(FLAGS) $^ -o $@

debug: libppm.c
	$(CC) $(FLAGS) -g $^ -o libppm

%.o : %.c
	$(CC) $(FLAGS) -c $< 

clean:
	rm -rf ./*.o
	rm -rf ./*.ppm
